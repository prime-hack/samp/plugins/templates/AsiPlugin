#ifndef MAIN_H
#define MAIN_H

#include "loader/loader.h"
#include <SRDescent/SRDescent.h>

class AsiPlugin : public SRDescent {
public:
	explicit AsiPlugin();
	virtual ~AsiPlugin();
};

#endif // MAIN_H
