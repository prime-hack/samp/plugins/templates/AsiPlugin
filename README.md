# AsiPlugin

Шаблон asi-плагинов основанный на CMake.

### [Инструкции по использованию шаблона](https://gitlab.com/prime-hack/samp/plugins/templates/AsiPlugin/-/wikis/%D0%98%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BA%D1%86%D0%B8%D0%B8-%D0%BF%D0%BE-%D0%B8%D1%81%D0%BF%D0%BE%D0%BB%D1%8C%D0%B7%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8E-%D1%88%D0%B0%D0%B1%D0%BB%D0%BE%D0%BD%D0%B0)

## Требования

* IDE (для создания проекта)
  * [VisualStudio](https://visualstudio.microsoft.com/ru/thank-you-downloading-visual-studio/?sku=Community&rel=16)
  * [QtCreator](https://download.qt.io/official_releases/qtcreator/4.15/4.15.0/qt-creator-opensource-windows-x86_64-4.15.0.exe)
  * [CLion](https://download.jetbrains.com/cpp/CLion-2021.1.3.exe)
* Компилятор с поддержкой C++20:
  * [MinGW](https://osdn.net/projects/mingw/) (рекомендуется) ([Не официальные сборки](https://winlibs.com/), где всегда последняя версия gcc)
  * [LLVM MinGW (Clang)](https://github.com/mstorsjo/llvm-mingw/releases) (не поддерживает кодировку CP1251, только UTF-8. Зато есть поддержка классических PDB файлов для отладки)
  * MSVC v140 или новее (ссылки нет, ибо поставляется в составе VS)
* [CMake](https://cmake.org/download/) 3.0 или новее (есть в составе VS)
* [git](https://git-scm.com/downloads) (для загрузки подмодулей. Есть в составе VS, но не регистрируется в $PATH, по этому все равно ставить надо)

### Опционально

* [Boost](https://www.boost.org/users/download/) 1.60 или новее (для работы корутин в подмодуле SRSignal)
* [UPX](https://github.com/upx/upx/releases) (для сжатия плагина, должен быть в $PATH)

